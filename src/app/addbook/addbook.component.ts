import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent implements OnInit {

  
  title:string;
  id:string;
  author:string;
  isedit:boolean = false;
  buttonText:string = "Add book" 
  userid:string;

  constructor(private bookservice:BooksService,public auth:AuthService,private router: Router,private route:ActivatedRoute) { }

  onSubmit(){
    if(this.isedit){
      this.bookservice.updateBook(this.userid,this.id,this.title,this.author);
      this.router.navigate(['/Books'])
    }else{
    this.bookservice.addBooks(this.userid,this.title,this.author);
    this.router.navigate(['/Books'])
    }
  }
  
  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.auth.user.subscribe(
      user => {
        this.userid = user.uid;
        if(this.id){
          this.isedit =true;
          this.buttonText ="Update";
          this.bookservice.getbook(this.userid,this.id).subscribe(
            book=>{
              this.author = book.data().author;
              this.title = book.data().title;
              console.log(this.author);
            }
            )
          }
        }
      )
  }

}
