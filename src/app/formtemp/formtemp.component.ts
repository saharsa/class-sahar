import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formtemp',
  templateUrl: './formtemp.component.html',
  styleUrls: ['./formtemp.component.css']
})
export class FormtempComponent implements OnInit {

  constructor(private router: Router) { }

  cities:object[] = [{id:1, name:'Tel-Aviv'},{id:2, name:'London'},{id:3, name:'Paris'}];
  temperature:number;
  city:string;

  onSubmit(){
    this.router.navigate(['/temp',this.temperature,this.city])
  }

  ngOnInit() {
  }

}
