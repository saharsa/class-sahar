import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { Identifiers } from '@angular/compiler';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  constructor(private bookserv:BooksService,public auth:AuthService) { }
  

  books$:Observable<any>;
  userId:string;
  
  delete(bookid:string){
    this.bookserv.deleteBook(this.userId,bookid);
  }

  ngOnInit() {
    //this.books$ = this.bookserv.getBooks();
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
    this.books$ = this.bookserv.getBooks(this.userId);
   // this.bookservice.addBooks();
       }
    )
  }

}
