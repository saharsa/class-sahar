import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

import {MatExpansionModule} from '@angular/material/expansion';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';

import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { BooksComponent } from './books/books.component';
import { RouterModule, Routes } from '@angular/router';
import { TemperatureComponent } from './temperature/temperature.component';
import { FormtempComponent } from './formtemp/formtemp.component';

// Firebase
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';


import { AddbookComponent } from './addbook/addbook.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';


const appRoutes: Routes = [
  { path: 'Books', component: BooksComponent },
  { path: 'tempform', component: FormtempComponent },
  { path: 'temp/:tp/:city', component: TemperatureComponent },
  { path: 'addbook', component: AddbookComponent },
  { path: 'addbook/:id', component: AddbookComponent},
  { path: 'singup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  //{ path: 'editauthor/:id', component: EditauthorComponent },
  { path: '',
  redirectTo: '/Books',
  pathMatch: 'full'
},
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    TemperatureComponent,
    FormtempComponent,
    AddbookComponent,
    SignupComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    HttpClientModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'sahar-class'),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
